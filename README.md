# com-data-mod

Ensemble de modules Drupal utilisant les données de Bordeaux Métropole.

## Modules

### com_jourscollecte_mod

Ce module permet de déterminer les jours de collecte des ordures ménagères à partir d'une simple adresse postale.
