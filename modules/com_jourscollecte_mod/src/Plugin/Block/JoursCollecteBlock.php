<?php

namespace Drupal\com_jourscollecte_mod\Plugin\Block;

use Drupal\Core\Block\BlockBase;

/**
 * Provides a custom block that contains your form.
 *
 * @Block(
 *   id = "com_jourscollecte_mod_block",
 *   admin_label = @Translation("Jours de collecte des déchets - Block")
 * )
 */
class JoursCollecteBlock extends BlockBase
{

  /**
   * {@inheritdoc}
   */
  public function build()
  {
    $form = \Drupal::formBuilder()->getForm('Drupal\com_jourscollecte_mod\Form\JoursCollecteForm');
    return $form;
  }

}
