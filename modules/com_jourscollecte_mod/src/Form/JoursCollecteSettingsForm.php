<?php

namespace Drupal\com_jourscollecte_mod\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class JoursCollecteSettingsForm. The config form for the com_jourscollecte_mod module.
 *
 * @package Drupal\com_jourscollecte_mod\Form
 */
class JoursCollecteSettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'com_jourscollecte_mod_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['com_jourscollecte_mod.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('com_jourscollecte_mod.settings');

    $form['api_key_name'] = [
      '#type' => 'textfield',
      '#title' => $this->t('API key name'),
      '#default_value' => $config->get('api_key_name') ?? 'bm_data',
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->config('com_jourscollecte_mod.settings');

    $config
      ->set('api_key_name', $form_state->getValue('api_key_name'))
      ->save();

    parent::submitForm($form, $form_state);
  }

}
