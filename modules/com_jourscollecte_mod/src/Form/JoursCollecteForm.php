<?php

namespace Drupal\com_jourscollecte_mod\Form;

use Drupal;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use proj4php\Point;
use proj4php\Proj;
use proj4php\Proj4php;

/**
 * Provides a default form.
 */
class JoursCollecteForm extends FormBase {

  const URL_GEOLOC = 'https://data.bordeaux-metropole.fr/wps?key=%s&service=wps&version=1.0.0&request=Execute&Identifier=geocodeur&DataInputs=input=%s';
  const URL_JOURS_COLLECTE = 'https://data.bordeaux-metropole.fr/geojson/features/EN_FRCOL_S?crs=epsg:3945&filter={"GEOM": { "$geoWithin": {  "$center" : [%s,%s] }  } }&attributes=["GID","TYPE","JOUR_COL","PASSAGE"]&key=%s';

  /**
   * @return mixed
   */
  private function getApiKey() {
    $config = $this->config('com_jourscollecte_mod.settings');
    $key_id = $config->get('api_key_name') ?? 'bm_data';
    return Drupal::service('key.repository')->getKey($key_id)->getKeyValue();
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'jourscollecte_form';
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    // Since FormBase uses service traits, we can inject these services without adding our own __construct() method.
    $form = new static($container);
    $form->setStringTranslation($container->get('string_translation'));
    $form->setMessenger($container->get('messenger'));
    return $form;
  }

  public function CC45ToWgs84($x, $y) {
    $proj4 = new Proj4php();

    // Create two different projections.
    $projWGS84 = new Proj('EPSG:4326', $proj4);
    $projCC45 = new Proj('EPSG:3945', $proj4);

    // Create a point.
    $pointSrc = new Point($x, $y, $projCC45);

    // Transform the point between datums.
    $pointDest = $proj4->transform($projWGS84, $pointSrc);
    return $pointDest->toArray();
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, $no_js_use = FALSE) {
    // We want to deal with hierarchical form values.
    $form['#tree'] = TRUE;

    $form['step'] = [
      '#type' => 'value',
      '#value' => !empty($form_state->getValue('step')) ? $form_state->getValue('step') : 1,
    ];

    if ($form['step']['#value'] == 2) {
      // Etape 2 : selection de l'adresse dans une liste déroulante.
      $options = $this->getOptionsAddress($form_state);

      if (count($options) > 0) {
        $limit_validation_errors = [['step'], ['step1']];
        $form['step1'] = [
          '#type' => 'value',
          '#value' => $form_state->getValue('step1'),
        ];
        $form['step2'] = [
          '#type' => 'fieldset',
          '#title' => $this->t('Step 2 : Select your address'),
        ];

        $form['step2']['select_options'] = [
          '#access' => true,
          '#type' => 'select',
          '#options' => $options,
          '#title' => $this->t('Select an address'),
          '#prefix' => '<div id="select-options-wrapper">',
          '#suffix' => '</div>',
        ];
      } else {
        $form_state->setValue('step', $form_state->getValue('step') - 1);
        $form['step']['#value'] = 1;

        $this->messenger()->addError($this->t('Address not found.'));
      }
    }

    if ($form['step']['#value'] == 1) {
      // Etape 1 : saisie de l'adresse.
      $limit_validation_errors = [['step']];
      $form['step1'] = [
        '#type' => 'fieldset',
        '#title' => $this->t('Step 1 : Enter your address'),
      ];
      $form['step1']['input_city'] = [
        '#type' => 'select',
        '#title' => $this->t('Select your city'),
        '#default_value' => $form_state->hasValue(['step1', 'input_city']) ? $form_state->getValue(['step1', 'input_city']) : '',
        '#required' => TRUE,
        '#options' => [
          '33003' => $this->t('Ambarès-et-Lagrave'),
          '33004' => $this->t('Ambès'),
          '33013' => $this->t('Artigues-près-Bordeaux'),
          '33032' => $this->t('Bassens'),
          '33039' => $this->t('Bègles'),
          '33056' => $this->t('Blanquefort'),
          '33063' => $this->t('Bordeaux'),
          '33065' => $this->t('Bouliac'),
          '33075' => $this->t('Bruges'),
          '33096' => $this->t('Carbon-Blanc'),
          '33119' => $this->t('Cenon'),
          '33162' => $this->t('Eysines'),
          '33167' => $this->t('Floirac'),
          '33192' => $this->t('Gradignan'),
          '33069' => $this->t('Le Bouscat'),
          '33200' => $this->t('Le Haillan'),
          '33519' => $this->t('Le Taillan-Médoc'),
          '33249' => $this->t('Lormont'),
          '33273' => $this->t('Martignas-sur-Jalle'),
          '33281' => $this->t('Mérignac'),
          '33312' => $this->t('Parempuyre'),
          '33318' => $this->t('Pessac'),
          '33376' => $this->t('Saint-Aubin de Médoc'),
          '33434' => $this->t('Saint-Louis-de-Montferrand'),
          '33449' => $this->t('Saint-Médard-en-Jalles'),
          '33487' => $this->t('Saint-Vincent-de-Paul'),
          '33522' => $this->t('Talence'),
          '33550' => $this->t('Villenave-d’Ornon'),
        ],
      ];
      $form['step1']['input_street'] = [
        '#type' => 'textfield',
        '#title' => $this->t('Enter your address'),
        '#default_value' => $form_state->hasValue(['step1', 'input_street']) ? $form_state->getValue(['step1', 'input_street']) : '',
        '#size' => '60',
        '#required' => TRUE,
        '#attributes' => [
          'placeholder' => t('Enter your address here')
        ]
      ];
    }

    if ($form['step']['#value'] == 3) {
      // Etape 3 : calcul des jours de collecte depuis l'adresse selectionnée.
      $limit_validation_errors = [['step'], ['step1'], ['step2']];
      $form['step1'] = [
        '#type' => 'value',
        '#value' => $form_state->getValue('step1'),
      ];
      $form['step2'] = [
        '#type' => 'value',
        '#value' => $form_state->getValue('step2'),
      ];
      $form['step3'] = [
        '#type' => 'fieldset',
        '#title' => $this->t('Step 3 : Garbage collection days'),
      ];

      $coord = $form_state->getValue(['step2', 'select_options']);

      $coordExploded = explode('_', $coord);

      $latitude = $coordExploded[0];
      $longitude = $coordExploded[1];

      $garbage_days_ar = $this->getJoursCollecte($latitude, $longitude);
      $garbage_days_str = "";

      if (empty($garbage_days_ar)) {
        $garbage_days_str = t('Garbage collection days not found');
      } else {
        $garbage_days_str = t("Garbage collection days result title");

        $garbage_days_str .= "<br />";

        foreach ($garbage_days_ar as $cleJourCollecte => $jourCollecte) {
          $cleJourCollecteComplete = "jours_collecte_cle_" . $cleJourCollecte;

          $garbage_days_str .= sprintf(
            "%s : %s <br />",
            t($cleJourCollecteComplete),
            strtolower(implode(', ', $jourCollecte))
          );

        }
      }

      $form['step3']['jourscollecte'] = [
        '#type' => 'markup',
        '#markup' => $garbage_days_str,
      ];
    }

    $form['actions'] = ['#type' => 'actions'];
    if ($form['step']['#value'] > 1) {
      $form['actions']['prev'] = [
        '#type' => 'submit',
        '#value' => $this->t('Previous step'),
        '#limit_validation_errors' => $limit_validation_errors,
        '#submit' => ['::prevSubmit'],
        '#ajax' => [
          'wrapper' => 'jours-collecte-wrapper',
          'callback' => '::prompt',
        ],
      ];
    }

    if ($form['step']['#value'] != 3) {
      if ($form['step']['#value'] == 1) {
        $valueBtbSubmit = $this->t('Next step');
      } else if ($form['step']['#value'] == 2) {
        $valueBtbSubmit = $this->t('Validate address');
      }

      $form['actions']['next'] = [
        '#type' => 'submit',
        '#value' => $valueBtbSubmit,
        '#submit' => ['::nextSubmit'],
        '#ajax' => [
          'wrapper' => 'jours-collecte-wrapper',
          'callback' => '::prompt',
        ],
      ];
    }

    $form['#prefix'] = '<div id="jours-collecte-wrapper">';
    $form['#suffix'] = '</div>';

    return $form;
  }

  /**
   * Wizard callback function.
   *
   * @param array $form
   *   Form API form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   Form API form.
   *
   * @return array
   *   Form array.
   */
  public function prompt(array $form, FormStateInterface $form_state) {
    return $form;
  }

  public function getJoursCollecte($latitude, $longitude) {
    $jourscollecte = [];
    $latlong = $this->CC45ToWgs84($latitude, $longitude);
    $longitude = $latlong[0];
    $latitude = $latlong[1];

    $api_key = $this->getApiKey();
    $url = sprintf(self::URL_JOURS_COLLECTE, $longitude, $latitude, urlencode($api_key));

    $optionsHttp = ['headers' => ['Accept' => 'application/json ']];
    $httpClient = \Drupal::httpClient();

    $response = $httpClient->get($url, $optionsHttp);
    $statusCode = $response->getStatusCode();

    if ($statusCode != 200) {
      $this->messenger()->addError($this->t('Unable to find the garbage collection days.'));
    } else {
      $body = (string)$response->getBody();
      $json = json_decode($body, true);

      foreach ($json['features'] as $feature) {
        $type = $feature['properties']['type'];
        $jourscollecte[$type] = $feature['properties']['jour_col'];
      }
    }

    return $jourscollecte;
  }

  public function getOptionsAddress($form_state) {
    $options = [];

    $city = $form_state->getValue(['step1', 'input_city']);
    $address = $form_state->getValue(['step1', 'input_street']);

    $api_key = $this->getApiKey();
    $pathToEncode = $address . ";commune=" . $city;
    $url = sprintf(self::URL_GEOLOC, urlencode($api_key), urlencode($pathToEncode));

    $optionsHttp = ['headers' => ['Accept' => 'application/xml']];
    $httpClient = \Drupal::httpClient();

    $response = $httpClient->get($url, $optionsHttp);
    $statusCode = $response->getStatusCode();

    if ($statusCode != 200) {
      $this->messenger()->addError($this->t('Impossible de rechercher cette adresse.'));
    } else {
      $body = (string)$response->getBody();
      $xml = simplexml_load_string($body);

      $bmNamespace = $xml->getNamespaces(true)['bm'];
      $gmlNamespace = $xml->getNamespaces(true)['gml'];

      $xml->registerXPathNamespace('bm', $bmNamespace);
      $xml->registerXPathNamespace('gml', $gmlNamespace);

      $addresses = $xml->xpath('//bm:default');
      foreach ($addresses as $address) {
        $children = $address->children($bmNamespace);
        $gid = $children->GID;

        if ($gid != "") {
          $number = $children->NUMERO;
          $nomVoie = $children->NOM_VOIE;
          $city = $children->COMMUNE;
          $coord = $children->geometry->children($gmlNamespace)->Point->children($gmlNamespace)->pos->__toString();
          $coord = str_replace(" ", "_", $coord);

          $ligneAddress = $number . " " . $nomVoie . ", " . $city;

          $options[$coord] = $ligneAddress;
        }
      }
    }
    return $options;
  }

  /**
   * Ajax callback that moves the form to the next step and rebuild the form.
   *
   * @param array $form
   *   The Form API form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The FormState object.
   *
   * @return array
   *   The Form API form.
   */
  public function nextSubmit(array $form, FormStateInterface $form_state) {
    $form_state->setValue('step', $form_state->getValue('step') + 1);
    $form_state->setRebuild();

    return $form;
  }

  /**
   * Ajax callback that moves the form to the previous step.
   *
   * @param array $form
   *   The Form API form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The FormState object.
   *
   * @return array
   *   The Form API form.
   */
  public function prevSubmit(array $form, FormStateInterface $form_state) {
    $form_state->setValue('step', $form_state->getValue('step') - 1);
    $form_state->setRebuild();

    return $form;
  }

  /**
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

  }
}
